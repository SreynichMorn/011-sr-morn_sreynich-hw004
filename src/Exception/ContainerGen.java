package Exception;
import com.sun.jdi.request.DuplicateRequestException;
import java.util.ArrayList;
import java.util.List;
class ContainerGen<T>{
    List<T> list=new ArrayList<T>();
    public ContainerGen(){}
    public void addItem(T value){
        try{
            value.toString();
            if(list.contains(value)){
                throw new DuplicateRequestException("Can not input duplicate value  "+ value);
            }
            else{
                list.add(value);
            }
        }catch (NullPointerException e) {
            throw new NullPointerException("Item can not be null ");
        }
    }
    public T getItem(int index){
        return list.get(index);
    }

    public int size() {
        return list.size();
    }

}